import {makeAutoObservable} from "mobx";
import {AxiosError} from "axios";
import {Transaction, UserData} from "./types";
import {persist} from "mobx-persist";
import Api from "./api";

class Store {
    constructor() {
        makeAutoObservable(this);
    }

    get token() { return this._token }

    get error() { return this._error }

    get userData() { return this._userData }

    get transactions() { return this._transactions }

    private makeRequest: <T>(request: ((...args: any[]) => Promise<T>), ...args: any[]) => Promise<T | void>
        = (request, ...args) => {
        return request && request(...args).catch((e: AxiosError) => this.setError(e));
    };

    logout = () => {
        this.setToken("");
        this.setUserData(undefined);
        this.api.onLogout();
    };

    getUserTransactions = async () => {
        const res = await this.makeRequest(this.api.getUserTransactions);
        if (res && res.data) this.setTransactions(res.data.trans_token);
    };

    getUserInfo = async () => {
        const res = await this.api.getUserInfo();
        if (res && res.data) this.setUserData(res.data.user_info_token);
    };

    sendWings = async (name: string, amount: number) => {
        const res = await this.makeRequest(this.api.sendWings, name, amount);
        if (res && res.data) {
            if (appStore.userData) appStore.userData.balance = appStore.userData.balance - amount;
        }
    };

    register = async (email: string, username: string, password: string) => {
        const res = await this.makeRequest(this.api.register, email, username, password);
        if (res && res.data) {
            appStore.setToken(res.data["id_token"]);
        }
    };

    login = async (email: string, password: string): Promise<void> => {
        const res = await this.makeRequest(this.api.login, email, password);
        if (res && res.data) {
            this.setToken(res.data["id_token"]);
        }
    };

    setTransactions = (data: Transaction[]) => {
        this._transactions = data;
    };

    setUserData = (data: UserData | undefined) => {
        this._userData = data;
    };

    setError = (newValue: AxiosError) => {
        this._error = newValue;
    };

    setToken = (newValue: string) => {
        this._token = newValue;
    };

    api = new Api();

    @persist
    private _token: string | undefined;

    @persist('object')
    private _error: AxiosError | undefined;

    @persist('object')
    private _userData: UserData | undefined;

    @persist('list')
    private _transactions: Transaction[] | undefined;
}

const appStore = new Store();

export default appStore;
