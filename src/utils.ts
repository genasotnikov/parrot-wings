import {useMediaQuery} from "@material-ui/core";

export const isDeskTopMedia = '(min-width: 600px)';

export const useIsDesktop = () => useMediaQuery(isDeskTopMedia)
