export type BaseUser = {
    id: number,
    name: string,
};

export interface UserData extends BaseUser {
    email: string,
    balance: number
}

export type Transaction = {
    id: number,
    date: Date,
    username: string,
    amount: number,
    balance: number
};

export type UserTransactionToken = {
    trans_token: Transaction[]
};

export type LoginResponse = { id_token: string };

export type UserInfoResponse = {
    user_info_token: UserData
};
