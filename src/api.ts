import axios, {AxiosInstance, AxiosResponse} from "axios";
import {BaseUser, LoginResponse, UserInfoResponse, UserTransactionToken} from "./types";

type Response<T> = Promise<AxiosResponse<T> | void>;

interface IApi {
    login: (email: string, password: string) => Response<LoginResponse>;
    register: (email:string, username: string, password: string) => Response<LoginResponse>;
    onLogout: () => void;
    getUserInfo: () => Response<UserInfoResponse>;
    getFilteredUsers: (filter: string) => Response<BaseUser[]>;
    getUserTransactions: () => Response<UserTransactionToken>;
    sendWings: (name: string, amount: number) => Response<any>;
}

class Api implements IApi {

    constructor() {
        this.instance = axios.create({
            baseURL: 'http://193.124.114.46:3001',
            headers: { "Content-Type": "application/json" },
        });
        const storage = localStorage.getItem("store");
        const token = storage && (JSON.parse(storage) as any)._token;
        if (token) this.createLoggedInstance(token);
    }

    getUserInfo = () =>
        this.loggedInstance ? this.loggedInstance.get("/api/protected/user-info/") : Promise.reject();

    getFilteredUsers = (filter: string) =>
        this.loggedInstance ? this.loggedInstance.post("/api/protected/users/list/", { filter }) : Promise.reject();

    getUserTransactions = () =>
        this.loggedInstance ? this.loggedInstance.get("/api/protected/transactions/") : Promise.reject();

    login = (email: string, password: string) =>
        this.instance.post("/sessions/create/", { email, password })
            .then((res: AxiosResponse<LoginResponse>) => {
                this.createLoggedInstance(res.data.id_token);
                return res;
            });

    onLogout = () => this.loggedInstance = undefined;

    register = (email: string, username: string, password: string) =>
        this.instance.post("/users/", { email, username, password })
            .then((res: AxiosResponse<LoginResponse>) => {
                this.createLoggedInstance(res.data.id_token);
                return res;
            });

    sendWings = (name: string, amount: number) =>
        this.loggedInstance ? this.loggedInstance.post("/api/protected/transactions/", { name, amount }) : Promise.reject();

    private createLoggedInstance = (token: string) => {
        this.loggedInstance = axios.create({
            baseURL: 'http://193.124.114.46:3001',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`,
            },
        });
    };

    private instance: AxiosInstance;
    private loggedInstance: AxiosInstance | undefined;
}

export default Api;
