import { createMuiTheme } from "@material-ui/core";
import { blue, grey } from "@material-ui/core/colors";
import perrotImg from "./assets/popugaj_ara_perya_okras_111129_2048x1152.jpg";

export default createMuiTheme({
    overrides: {
        MuiContainer: {
            fixed: {
                height: "100vh",
                maxWidth: "unset !important",
                backgroundImage: `url(${perrotImg})`,
                backgroundPosition: "top",
                padding: 0
            }
        },
        MuiFormControl: {
            root: {
                display: "flex",
                marginTop: 3
            }
        },
        MuiLink: {
            root: {
                cursor: "pointer"
            }
        },
        MuiButton: {
            root: {
                backgroundColor: blue["500"],
                color: "white",
                "&:hover, &:active, &:focus": {
                    backgroundColor: blue["400"],
                }
            }
        },
        MuiTableCell: {
            root: {
                cursor: "default"
            },
            head: {
                backgroundColor: grey["300"]
            }
        },
    },
});
