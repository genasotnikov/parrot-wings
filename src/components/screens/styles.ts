import {makeStyles} from "@material-ui/core/styles";
import { green } from '@material-ui/core/colors';

export default makeStyles({
    container: {
        position: "fixed",
        top: 0,
        left: 0,
        width: "100vw",
        color: "white",
        backgroundColor: green["600"],
        padding: 10,
    },
    background: {
        backgroundColor: "white",
        opacity: 0.8,
    },
    logOut: {
        color: "white"
    },
    loginContainer: {
        height: "100%"
     },
    registrationContainer: {
        borderRadius: "30%",
        alignContent: "center",
        height: "100%"
    },
    marginBottom: {
        marginBottom: 15
    },
    formContainer: {
        padding: 10,
        justifyContent: "center",
        backgroundColor: "white",
        maxWidth: 300,
        "@media (max-width:600px)": {
            alignSelf: "center",
        }
    }
});
