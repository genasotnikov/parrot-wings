import React from "react";
import {useIsDesktop} from "../../../../utils";
import {Grid, Link, Typography} from "@material-ui/core";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import useStyles from "./style";

const BackButton = () => {
    const classes = useStyles();
    const isDesktop = useIsDesktop();

    return <>{!isDesktop && (
       <Link href={"/home"} className={classes.link}>
           <Grid container alignItems="center">
               <ArrowBackIosIcon />
               <Typography style={{fontSize: "15pt", marginBottom: 0}} gutterBottom>
                   Back
               </Typography>
           </Grid>
       </Link>
    )}</>;
};

export default BackButton;
