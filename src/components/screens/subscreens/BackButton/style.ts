import {makeStyles} from "@material-ui/core/styles";
import { grey } from '@material-ui/core/colors';

export default makeStyles({
    link: {
        color: grey["500"]
    }
});
