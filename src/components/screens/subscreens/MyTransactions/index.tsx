import React, {useEffect} from "react";
import {Table, TableHead, TableRow, TableCell, TableBody, Grid, Typography} from "@material-ui/core";
import {observer} from "mobx-react";
import appStore from "../../../../store";
import BackButton from "../BackButton";
import useStyles from "./styles";

const MyTransactions = () => {
    const classes = useStyles();

    useEffect(() => {
        appStore.getUserTransactions();
    }, []);

    return (
        <Grid container direction="column">
            <BackButton/>
            {(!appStore.transactions || !appStore.transactions.length)
                ? <Typography component={"h1"} className={classes.noTransactions}>You don't have any transactions</Typography>
                : (
                    <Grid item xs={12} className={classes.transactions}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell><Typography>id</Typography></TableCell>
                                    <TableCell><Typography>date</Typography></TableCell>
                                    <TableCell><Typography>name</Typography></TableCell>
                                    <TableCell><Typography>amount</Typography></TableCell>
                                    <TableCell><Typography>balance</Typography></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {appStore.transactions.map((transaction) => (
                                    <TableRow>
                                        {Object.keys(transaction).map(key =>
                                            <TableCell>
                                                <Typography>{(transaction as any)[key]}</Typography>
                                            </TableCell>)}
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </Grid>
                )
            }
        </Grid>
    );
};

export default observer(MyTransactions);
