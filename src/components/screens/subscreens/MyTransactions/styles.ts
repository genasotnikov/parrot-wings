import {makeStyles} from "@material-ui/core/styles";
import { grey } from "@material-ui/core/colors";
import {isDeskTopMedia} from "../../../../utils";

export default makeStyles({
    transactions: {
        overflowY: "scroll",
        maxHeight: 500,
        [isDeskTopMedia]: {overflow: "scroll"}
    },
    noTransactions: {
        fontWeight: "bold",
        fontSize: 20,
        color: grey["500"],
        [isDeskTopMedia]: { fontSize: 50 }
    }
});
