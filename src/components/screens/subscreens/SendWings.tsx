import React, {useEffect, useState} from "react";
import {TextField, Grid, Button} from "@material-ui/core";
import {BaseUser} from "../../../types";
import {Alert, Autocomplete} from "@material-ui/lab";
import appStore from "../../../store";
import BackButton from "./BackButton";

type ValidationErrors = {
    selectedName?: string;
    amount?: string;
}

const SendWings = () => {
    const [filter, setFilter] = useState("");
    const [validationErrors, setValidationErrors] = useState<ValidationErrors>({});
    const [usersList, setUsersList] = useState<BaseUser[]>([]);
    const [amount, setAmount] = useState<number | undefined>(undefined);
    const [sentAmount, setSentAmount] = useState<number>(0);
    const [selectedName, setSelectedName] = useState<string>("");

    useEffect(() => {
        if (selectedName) setValidationErrors({ amount: validationErrors.amount });
    }, [selectedName]);

    useEffect(() => {
        if (amount) setValidationErrors({ selectedName: validationErrors.selectedName });
    }, [amount]);

    const isValid = () => {
        const errors: ValidationErrors = {};
        if (!selectedName) errors.selectedName = "is required";
        if (amount === undefined) errors.amount = "is required";
        if (amount === 0) errors.amount = "should be more than one";
        setValidationErrors(errors);
        return !(errors.amount || errors.selectedName);
    };

    const submit = async (e: any) => {
        e.preventDefault();
        setValidationErrors({});
        if (isValid() && amount) {
            await appStore.sendWings(selectedName, amount);
            setSentAmount(amount);
        }
    };

    useEffect(() => {
        if (filter) appStore.api.getFilteredUsers(filter).then((response) => setUsersList(response.data))
    }, [filter]);

    return (
        <>
            <BackButton/>
            <Grid container justify="center" alignContent="center" direction="column" style={{height: "50%"}}>
                <Autocomplete
                    options={usersList}
                    onChange={(e, value) => setSelectedName((value as BaseUser).name)}
                    getOptionLabel={(value: Partial<BaseUser>) => value && `${value.name} (id: ${value.id})`}
                    style={{ width: 300 }}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            label="Choose user"
                            error={!!validationErrors.selectedName}
                            helperText={validationErrors.selectedName}
                            variant="outlined"
                            placeholder="Enter user's name"
                            style={{marginBottom: 15}}
                            onChange={e => setFilter(e.target.value)}
                            value={filter}
                        />
                    )}
                />
                <TextField
                    value={amount}
                    style={{marginBottom: 15}}
                    variant="outlined"
                    error={!!validationErrors.amount}
                    helperText={validationErrors.amount}
                    label="Amount"
                    onChange={e => setAmount(Number(e.target.value))}
                    type="number" />
                {sentAmount ? <Alert severity="success">You successfully sent {sentAmount} wing{sentAmount > 1 && "s"}!</Alert> : null}
                <Button onClick={submit}>Send Wings</Button>
            </Grid>
        </>
    );
};

export default SendWings;
