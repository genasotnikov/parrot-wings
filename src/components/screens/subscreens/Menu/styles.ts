import {makeStyles} from "@material-ui/core/styles";
import { grey } from '@material-ui/core/colors';

export default makeStyles({
    menuItem: {
        cursor: "pointer",
        "&:hover, &:active": {
          backgroundColor: grey[400]
      }
    },
});
