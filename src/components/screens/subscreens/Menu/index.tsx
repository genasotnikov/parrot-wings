import React from "react";
import {Avatar, List, ListItem, ListItemAvatar, ListItemText} from "@material-ui/core";
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import PaymentIcon from '@material-ui/icons/Payment';
import {useHistory} from "react-router";
import useStyles from "./styles";

const Menu = () => {
  const history = useHistory();
  const classes = useStyles();

  return (
      <List>
          <ListItem className={classes.menuItem} onClick={() => history.push("/home/transactions")}>
              <ListItemAvatar>
                  <Avatar>
                      <AccountBalanceIcon />
                  </Avatar>
              </ListItemAvatar>
              <ListItemText>List of transactions</ListItemText>
          </ListItem>
          <ListItem className={classes.menuItem} onClick={() => history.push("/home/sendWings")}>
              <ListItemAvatar>
                  <Avatar>
                      <PaymentIcon />
                  </Avatar>
              </ListItemAvatar>
              <ListItemText>Send wings</ListItemText>
          </ListItem>
      </List>
  );
};

export default Menu;
