export interface IRegistrationFormValues {
    email: string;
    username: string;
    password: string;
    [key: string]: string | undefined;
}
