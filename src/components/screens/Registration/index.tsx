import React from "react";
import {FormLabel, Grid, Button, Link} from "@material-ui/core";
import { TextField, TextFieldProps } from "formik-material-ui";
import appStore from "../../../store";
import {useHistory} from "react-router";
import useStyles from "../styles";
import {Field, Form, Formik} from "formik";
import {IRegistrationFormValues} from "./types";
import validation from "./validation";

interface RegistrationProps {
    goToLogin?: () => void;
}

const Registration: React.FC<RegistrationProps> = ({ goToLogin }) => {
    const history = useHistory();
    const classes = useStyles();

    const submit = async (values: IRegistrationFormValues) => {
        const { email, username, password } = values;
        await appStore.register(email, username, password);
        if (appStore.token) history.push("/home");
    };

    const inputComponent = (props: TextFieldProps) => <TextField {...props} variant="outlined" />;
    const errorHeight = React.useMemo(() => 22, []);

    return (
        <Grid container justify="space-around" className={classes.registrationContainer}>
            <Grid container direction="column" className={`${classes.formContainer} ${classes.background}`}>
                <Formik initialValues={{email: "", username: "", password: ""}}
                        validate={validation}
                        onSubmit={submit}>
                    {({errors, touched}) => (
                        <Form>
                            <FormLabel className={classes.marginBottom}>Registration</FormLabel>
                            <Field
                                component={inputComponent}
                                label="Email"
                                name="email"
                            />
                            {!(errors?.email && touched.email) && <div style={{height: errorHeight}} />}
                            <Field
                                component={inputComponent}
                                label="Username"
                                name="username"
                            />
                            {!(errors?.username && touched.username) && <div style={{height: errorHeight}} />}
                            <Field
                                component={inputComponent}
                                label="Password"
                                type="password"
                                name="password"
                            />
                            {!(errors?.password && touched.password) && <div style={{height: errorHeight}} />}
                            <Button type="submit">Submit</Button>
                        </Form>
                    )}
                </Formik>
                <Link onClick={goToLogin} href={goToLogin ? undefined : "/startPage"}>Login!</Link>
            </Grid>
        </Grid>
    );
};

export default Registration;
