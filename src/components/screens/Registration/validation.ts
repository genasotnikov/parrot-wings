import {IRegistrationFormValues} from "./types";

const emailRefExp = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

const isEmptyError = (values: IRegistrationFormValues, errors: Partial<IRegistrationFormValues>, key: string) => {
    if (!values[key]) errors[key] = 'required';
};

const validateEmail = (values: IRegistrationFormValues, errors: Partial<IRegistrationFormValues>) => {
    isEmptyError(values, errors, "email");
    if (!errors.email && !emailRefExp.test(values.email))
        errors.email = 'Invalid email address';
};

const validatePassword = (values: IRegistrationFormValues, errors: Partial<IRegistrationFormValues>) => {
    isEmptyError(values, errors, "password");
    if (!errors.password && values.password.length < 8)
        errors.password = 'The password should contain at least 8 symbols';
};

export default (values: IRegistrationFormValues) => {
    const errors: Partial<IRegistrationFormValues> = {};
    validateEmail(values, errors);
    isEmptyError(values, errors, "username");
    validatePassword(values, errors);
    return errors;
}
