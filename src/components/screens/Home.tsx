import React, {useEffect} from "react";
import {observer} from "mobx-react";
import {Redirect, Switch} from "react-router";
import Menu from "./subscreens/Menu";
import appStore from "../../store";
import {Grid} from "@material-ui/core";
import Header from "../Header";
import MyTransactions from "./subscreens/MyTransactions";
import SendWings from "./subscreens/SendWings";
import {useIsDesktop} from "../../utils";
import PrivateRoute from "../utils/PrivateRoute";
import grey from "@material-ui/core/colors/grey";

const Home = () => {
    const isDeskTop = useIsDesktop();

    useEffect(() => {
        if (appStore.token) appStore.getUserInfo();
    }, []);

    if (!appStore.token) return <Redirect to="/startPage" />;

    return (
        <Grid style={{height: "100%", background: "white"}}>
            <Header />
            <Grid container style={{height: "calc(100% - 70px)"}}>
                {isDeskTop && <Grid item md={3} style={isDeskTop ? { borderRight: `1px solid ${grey["300"]}` } : {}}><Menu /></Grid>}
                <Grid item md={9} xs={12}>
                    <Switch>
                        <PrivateRoute path="/home/sendWings" component={SendWings} />
                        <PrivateRoute path="/home/transactions" component={MyTransactions} />
                        {!isDeskTop && <PrivateRoute path="/" component={Menu} />}
                    </Switch>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default observer(Home);
