import {useHistory} from "react-router";
import React, {useState} from "react";
import appStore from "../../store";
import {Button, FormLabel, Grid, TextField} from "@material-ui/core";
import useStyles from "./styles";
import {useIsDesktop} from "../../utils";
import Registration from "./Registration";

const Login = () => {
    const history = useHistory();
    const [isRegister, setIsRegister] = useState<boolean>(false);
    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const isDesktop = useIsDesktop();
    const classes = useStyles();

    const submit = async (e: any) => {
        e.preventDefault();
        await appStore.login(email, password);
        if (appStore.token) history.push("/home");
    };

    if (isRegister) return <Registration goToLogin={() => setIsRegister(false)} />

    return (
        <Grid container justify={isDesktop ? "flex-end" : "space-around"} className={classes.loginContainer}>
            <Grid container direction="column" justify="space-around" className={`${classes.formContainer} ${classes.background}`}>
                <FormLabel className={classes.marginBottom}>Login</FormLabel>
                <TextField
                    onChange={e => setEmail(e.target.value)}
                    value={email} placeholder={"example@mail.com"}
                    className={classes.marginBottom}
                    variant="outlined"
                    label="Email" />
                <TextField
                    onChange={e => setPassword(e.target.value)}
                    variant="outlined"
                    className={classes.marginBottom}
                    label="password"
                    type="password"
                    value={password} />
                <Grid container justify="space-between">
                    <Button type="submit" onClick={submit}>Submit</Button>
                    <Button onClick={() => setIsRegister(true)}>
                        Registration
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Login;
