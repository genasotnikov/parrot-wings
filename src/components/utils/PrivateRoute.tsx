import React from "react";
import {Redirect, Route, RouteProps} from "react-router";
import appStore from "../../store";

const PrivateRoute: React.FC<RouteProps> = ({ children, ...rest }) => {
    return (
        <Route
            {...rest}
            render={({ location }) =>
                appStore.token
                    ? children
                    : (
                    <Redirect
                        to={{
                            pathname: "/startPage",
                            state: { from: location }
                        }}
                    />
                )
            }
        />
    );
}

export default PrivateRoute;
