import {makeStyles} from "@material-ui/core/styles";
import { green } from '@material-ui/core/colors';

export default makeStyles({
    container: {
        width: "100vw",
        color: "white",
        backgroundColor: green["600"],
        padding: 10,
        height: 70,
        opacity: 1
    },
    logOut: {
        color: "white"
    },
});
