import React from "react";
import {Grid, IconButton, Typography} from "@material-ui/core";
import {observer} from "mobx-react";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import parrotSvg from "../../assets/427487.svg";
import appStore from "../../store";
import {useHistory} from "react-router";
import useStyles from "./styles";

const Header = () => {
    const { balance, name } = appStore.userData || {};
    const history = useHistory();
    const classes = useStyles();

    const logout = () => {
        appStore.logout();
        history.push("/startPage");
    };

    return (
        <Grid container item className={classes.container} xs={12}>
            <Grid container alignItems="center" item xs={3} md={9}>
                <img src={parrotSvg} style={{marginLeft: "1vw"}} width={30} height={30}  alt="logo" />
            </Grid>
            <Grid xs={4} md={1} item container direction="column" justify="space-around">
                <Typography gutterBottom>
                    Balance: {balance}
                </Typography>
            </Grid>
            <Grid xs={3} md={1} item container direction="column" justify="space-around">
                <Typography gutterBottom>
                    Hi {name}!
                </Typography>
            </Grid>
            <Grid xs={2} md={1} item><IconButton onClick={logout}><ExitToAppIcon className={classes.logOut} /></IconButton></Grid>
        </Grid>
    );
};

export default observer(Header);
