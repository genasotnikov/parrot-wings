import React, {useEffect, useState} from 'react';
import {Container, Snackbar, CircularProgress, MuiThemeProvider} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import {Redirect, Route, Switch} from "react-router";
import { BrowserRouter } from "react-router-dom";
import {Provider, observer} from "mobx-react";
import appStore from "./store";
import Registration from "./components/screens/Registration";
import Login from "./components/screens/Login";
import Home from "./components/screens/Home";
import {create} from "mobx-persist";
import theme from "./theme";
import "./rootStyles.css";
import 'fontsource-roboto';
import PrivateRoute from "./components/utils/PrivateRoute";


const hydrate = create({
    jsonify: true,
});

function App() {
    const [isRehydrated, setIsRehydrated] = useState(false);
    const [errorVisible, setErrorVisible] = useState<boolean>();

    const anyError = appStore.error;

    useEffect(() => {
        hydrate("store", appStore)
            .then(() => setIsRehydrated(true))
            .catch((e) => {throw e});
    }, []);


    useEffect(() => {
        if (anyError) {
            setErrorVisible(true);
            setTimeout(() => {
                setErrorVisible(false);
            }, 5000);
        }
    },[anyError]);

    if (!isRehydrated) return <CircularProgress />;

  return (
      <Provider store={appStore}>
          <MuiThemeProvider theme={theme}>
              <Container fixed={true}>
                  <BrowserRouter>
                      <Switch>
                          <Route path="/registration" component={Registration} />
                          <Route path="/startPage" component={Login} />
                          <PrivateRoute path="/home" component={Home} />
                          <PrivateRoute path="/" component={() => <Redirect to="/home" />} />
                      </Switch>
                  </BrowserRouter>
                  <Snackbar open={errorVisible} autoHideDuration={5000} onClose={() => setErrorVisible(false)}>
                      <Alert severity="error">
                          {appStore && appStore.error && appStore.error.response && appStore.error.response.data}
                      </Alert>
                  </Snackbar>
              </Container>
          </MuiThemeProvider>
      </Provider>
  );
}

export default observer(App);

